const { ApolloServer } = require('apollo-server');
const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient()

const fs = require('fs');
const path = require('path');
let idCount = 0;

const resolvers = {
  Query: {
    getData: async () => { return (await prisma.data.findFirst()).data },
    getMessages: async () => { return prisma.messages.findMany() }

  },
  // Link: {
  //   id: (parent) => parent.id,
  //   description: (parent) => parent.description,
  //   url: (parent) => parent.url,
  // },
  Mutation: {
    // 2
    sendMessage: async (parent, args) => {
      console.log("celled");
      return addMessagetoDb({
        message: args.message,
        email: args.email
      });
    }
    ,

    sendData: async (parent, args) => {
      return prisma.data.update(
        {
          where: {
            id: 0
          }
          ,
          data: {
            data: args.data
          }
        }
      );
    }


  }
}


// 3
const server = new ApolloServer({
  typeDefs: fs.readFileSync(
    path.join(__dirname, 'schema.graphql'),
    'utf8'
  )
  ,
  resolvers,
})

var server_port = process.env.YOUR_PORT || process.env.PORT || 80;
var server_host = process.env.YOUR_HOST || '0.0.0.0';
server.listen(server_port, server_host, function() {
    console.log('Listening on port %d', server_port);
});

async function addMessagetoDb(Message) {
  console.log(prisma)
  console.log(prisma.Message)
  const abc = await prisma.messages.create({
    data: Message
  })



}

function getDatafromDb() {
  const data = prisma.data.findFirst();
  return data;
}